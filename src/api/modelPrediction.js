import { post,get,postFrom,put,deleteFunc} from "@/utils/axios";
// 预测配置
export function getCategory() {
  return post("filter/qureyAll");
}

export function priceReg(param){
  return post("model/verification",param)
}

export function preResult(param){
  return post("model/prediction",param)
}

export function saveData(param){
  return post("model",param)
}

export function getLookModel(param){
  return get("model",param)
}

export function updateSave(param){
  return put("model",param)
}


// 配置列表
export function getlist(params){
  return get("model/list",params)
}
export function deleteModel(param){
  return deleteFunc("model",param)
}
export function getLabelList(params){
  return postFrom("model/queryLabel",params)
}
export function deleteLabel(params){
  return postFrom("model/deleteLabel",params)
}
export function addLabel(params){
  return postFrom("model/addLabel",params)
}
export function getTime(){
  return get("model/queryStockDate")
}

// 版本配置基本信息
export function getDefault(param){
  return get("model/config/default",param)
}


// 版本配置对比
export function download(param){
  return get("model/file",param)
}

export function comparisonFunc(params){
  return get("model/comparison",params)
}

// 预测结果汇总
export function summary(params){
  return postFrom("model/summary",params)
}

// 预测结果汇总
export function summaryTotals(params){
  return postFrom("model/summaryTotals",params)
}

// 配置限制条件
export function limit(){
  return get("model/limit")
}

