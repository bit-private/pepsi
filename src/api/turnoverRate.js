import { post } from "@/utils/axios";

//看板主数据查询
export function queryTurnoverData(params) {
    return post("turnover/queryTurnoverData", params);
  }
//看板子数据查询
export function queryTurnoverSubData(params) {
    return post("turnover/queryTurnoverSubData", params);
  }
export default queryTurnoverData;queryTurnoverSubData