import { postFrom } from "@/utils/axios";

//登录
export function login(params) {
  return postFrom("user/login", params);
}

export default login
