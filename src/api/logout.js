import { postFrom } from "@/utils/axios";

//登出
export function logout(params) {
  return postFrom("user/logout", params);
}
export default logout