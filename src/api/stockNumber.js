import { post } from "@/utils/axios";

//看板主数据查询
export function queryStockData(params) {
    return post("stock/queryStockData", params);
  }
  export function queryStockSubData(params) {
    return post("stock/queryStockSubData", params);
  }
  export default queryStockData;queryStockSubData