import { post } from "@/utils/axios";
// import { get } from "core-js/fn/dict";

//看板主数据查询
export function queryReportData(params) {
    return post("report/queryReportData", params);
  }