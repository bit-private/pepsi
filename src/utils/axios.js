import axios from "axios";
import qs from "qs";

// baseurl
axios.defaults.baseURL = process.env.VUE_APP_API_URL;
// http request 拦截器
axios.interceptors.request.use(
  config => {
    // config.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'
    // 判断是否存在token，如果存在的话，则每个http header都加上token
    if (sessionStorage.getItem("token")) {   
      config.headers['X-token'] = sessionStorage.getItem('token')
    }
    return config;
  },
  err => {
    return Promise.reject(err);
  }
);
// http response 拦截器
axios.interceptors.response.use(
  response => {
    // console.log(response,"response")
    // exVue.$router.push("/login")
    if(response.headers['x-token']){
      sessionStorage.setItem("token",response.headers['x-token'])
    }
    if(
        response.data.code==9092||
        response.data.code==9093
      ){
      // sessionStorage.removeItem("token")
      window.location.href = "/";
    }
    return response;
  },
  error => {
    if (error && error.response) {
      switch (error.response.status) {
        case 401:
          // 返回 401 清除token信息并跳转到登录页面
          // sessionStorage.removeItem("token", "");
          window.location.href = "/";
          break;
      }
    }
    // 返回接口返回的错误信息
    return Promise.reject(error.response.data);
  }
);

export function get(url, params) {
  return new Promise((resolve, reject) => {
    axios.get(url, {
        params
      })
      .then(
        res => {
          resolve(res.data);
        },
        err => {
          reject(err);
        }
      );
  });
}
export function put(url, params) {
  return new Promise((resolve, reject) => {
    // console.log(str)
    axios.put(url,params).then(
      response => {
        resolve(response.data);
      },
      err => {
        reject(err);
      }
    );
  });
}

export function deleteFunc(url, params) {
  return new Promise((resolve, reject) => {
    // console.log(str)
    axios.delete(url,{params}).then(
      response => {
        resolve(response.data);
      },
      err => {
        reject(err);
      }
    );
  });
}

export function post(url, params) {
  return new Promise((resolve, reject) => {
    // console.log(str)
    axios.post(url,params).then(
      response => {
        resolve(response.data);
      },
      err => {
        reject(err);
      }
    );
  });
}

export function postFormData(url, formData) {
  return new Promise((resolve, reject) => {
    axios({
      method: "post",
      url: url,
      headers: {
        "Content-Type": "multipart/form-data"
      },
      data: formData
    }).then(
      res => {
        resolve(res.data);
      },
      err => {
        reject(err);
      }
    );
  });
}

export function postFrom(url, params) {
    return new Promise((resolve, reject) => {
      // console.log(str)
      axios.post(url, qs.stringify(params, { arrayFormat: "comma" })).then(
        response => {
          resolve(response.data);
        },
        err => {
          reject(err);
        }
      );
    });
  }
