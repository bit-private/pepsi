import Vue from "vue";
import App from "./App.vue";
import creatRouter from "./router";
import addRoutes from "./router/addRoutes";
import creatStore from "./store";
import "./plugins/element.js";
import "./assets/common/css/reset.css";
import echarts from "echarts";
Vue.prototype.$echarts = echarts;
Vue.config.productionTip = false;

const router = creatRouter();
router.addRoutes(addRoutes);

const store = creatStore();

Vue.directive('tableScroll', {
  bind(el, binding, vnode, oldVnode) {
      let scrollTop = 0;
      let dom = el.querySelector(".el-table__body-wrapper");
      dom.addEventListener("scroll", (e) => {
          // let isBottom = (dom.scrollHeight).toFixed(0) - (dom.scrollTop).toFixed(0) === dom.clientHeight;
          let isBottom = (dom.scrollHeight).toFixed(0) - (dom.scrollTop).toFixed(0)-dom.clientHeight<=10
          let isHorizontal = scrollTop == dom.scrollTop;
          if (isBottom && !isHorizontal && dom.scrollTop != 0) {
              if (binding.value) {
                  binding.value()
              }
          }
          if (!isHorizontal) {
              scrollTop = dom.scrollTop;
          }
      });
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");


router.beforeEach((to, from, next)=>{
  if(to.path==="/" || to.path =="/login"){
    sessionStorage.removeItem("token")
    sessionStorage.removeItem("userName")
    next()
  }
  let token = sessionStorage.getItem("token")

  next()
})

