import * as types from "./mutation-types";

const mutations = {
  [types.SET_TOKEN](state, token) {
    sessionStorage.setItem("token", token);
    state.token = token;
  }
};

export default mutations;
