import * as types from "./mutation-types";

export const setData = ({ commit }, { token }) => {
  commit(types.SET_TOKEN, token);
};
