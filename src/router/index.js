import Vue from "vue";
import VueRouter from "vue-router";
import baseRoutes from "./baseRoutes";

Vue.use(VueRouter);

//获取原型对象上的push函数
const originalPush = VueRouter.prototype.push
//修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}

export default () => {
  return new VueRouter({
    // mode如果设置为history模式需要后台进行配合
    // 默认mode为hash。hash路由不会被搜索引擎解析,不利于seo优化
    mode: "hash",
    routes: baseRoutes
  });
};
