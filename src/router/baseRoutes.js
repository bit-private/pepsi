const Login = () => import("@/views/Login");

const baseRoutes = [
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "*",
    redirect: "/login"
  }
];

export default baseRoutes;
