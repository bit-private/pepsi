const Home = () => import("@/views/Home");
const salesNumber = () => import("@/views/salesNumber");
const stockNumber = () => import("@/views/stockNumber")
const turnoverRate=()=>import("@/views/turnoverRate")
const modelPrediction = () => import("@/views/modelPrediction")
const HistorConfig = ()=> import("@/components/modelPrediction/HistorConfig")
const ForecastConfig = ()=> import("@/components/modelPrediction/ForecastConfig")
const trackingReport=()=>import("@/views/trackingReport")
const DataSummary = ()=> import("@/components/modelPrediction/DataSummary")
const BasicInfor = ()=> import("@/components/modelPrediction/BasicInfor")
const ForecastSummary = ()=> import("@/components/modelPrediction/ForecastSummary")
const addRoutes = [
  {
    path: "/home",
    name: "Home",
    component: Home,
    children: [
      {
        path: "/salesNumber",
        name: "salesNumber",
        component: salesNumber,
        meta:["数据看板","产品销售数据"]
      },
      {
        path: "/stockNumber",
        name: "stockNumber",
        component: stockNumber,
        meta:["数据看板","KA库存数据"]
      },
      {
        path: "/turnoverRate",
        name: "turnoverRate",
        component: turnoverRate,
        meta:["数据看板","产品周转天数"]
      },
    ]
  },
  {
    path: "/trackingReport",
    name: "trackingReport",
    component: trackingReport,
    meta:["追踪报告",'追踪报告']
  },
  {
    path: "/modelPrediction",
    name: "modelPrediction",
    component:modelPrediction,
    children:[
      {
        path: "HistorConfig",
        name: "HistorConfig",
        component:HistorConfig,
        meta:["模型预测","配置版本列表"]
      },
      {
        path: "ForecastConfig",
        name: "ForecastConfig",
        component:ForecastConfig,
        meta:["模型预测","预测配置"]
      },
      {
        path: "DataSummary",
        name: "DataSummary",
        component:DataSummary,
        meta:["模型预测","配置版本对比"]
      },
      {
        path: "BasicInfor",
        name: "BasicInfor",
        component:BasicInfor,
        meta:["模型预测","填写配置版本基本信息"]
      },
      {
        path: "ForecastSummary",
        name: "ForecastSummary",
        component:ForecastSummary,
        meta:["模型预测","预测结果汇总"]
      },
    ]
  },

 
];
export default addRoutes;
