module.exports = {
  devServer: {
    open: true,
    host: "0.0.0.0", 
    port: 9000,
    https: false,
    hot: true,
    hotOnly: true,
    // proxy: {
    //   // 配置跨域
    //   "/api": {
    //     target: process.env.VUE_APP_API_URL,
    //     ws: true,
    //     changeOrigin: true,  //是否跨域
    //     pathRewrite: {
    //       "^/api": ""
    //     }
    //   }
    // }
  },
  pages: {
    index: {
      entry: "src/main.js",
      title: "PEPSICO"
    }
  }
  　

};
